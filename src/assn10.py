# Isaac Anderson
# CS 1400-1
# Assn 10
# Chessboard creation off user-input

#### Add Import Statement(s) as needed ####
from src.chessboard import drawChessboard

#### End Add Import Statement(s) ####

def main():
    #### Add Code to get input from user ####
    startX = int(input("Enter starting X-coordinate: "))
    startY = int(input("Enter starting Y-coordinate: "))
    width = input("Enter the width: ")
    height = input("Enter the height: ")
    #### End Add Code to get input from user ####

# Draw chessboard
    if width == "" and height == "":
        drawChessboard(startX, startY)
    elif height == "":
        drawChessboard(startX, startY, width=eval(width))
    elif width == "":
        drawChessboard(startX, startY, height=eval(height))
    else:
        drawChessboard(startX, startY, eval(width), eval(height))

# Call function to draw chessboard
main()