# Import turtle and define
import turtle
tr = turtle.Turtle()

# Function to draw one rectangle for the chessboard
def drawRectangle(height, width):
    rectHeight = height / 8
    rectWidth = width / 8
    tr.pendown()
    tr.color("Black")
    tr.begin_fill()
    tr.forward(rectWidth)
    tr.left(90)
    tr.forward(rectHeight)
    tr.left(90)
    tr.forward(rectWidth)
    tr.left(90)
    tr.forward(rectHeight)
    tr.left(90)
    tr.end_fill()

# Function to draw all rectangles on the chessboard
def drawAllRectangles(height, width, boardX, boardY):
    tr.speed(8)
    rectHeight = height / 8
    rectWidth = width / 8
    evenRectSpace = 0
    oddRectSpace = rectHeight

# Draw first rows of rectangles
    for j in range(4):
        evenRectSpace += rectHeight * 2
        for i in range(4):
            i = rectWidth * 2
            drawRectangle(height, width)
            tr.penup()
            tr.forward(i)
            tr.pendown()
        tr.penup()
        tr.goto(boardX, boardY + evenRectSpace)
        tr.pendown()

# Set up for new loop, draw rest of chessboard squares
    evenRectSpace = 0
    tr.penup()
    tr.goto(boardX + rectWidth, boardY + rectHeight)
    tr.pendown()

# Draw next rows of rectangles
    for a in range(4):
        oddRectSpace += rectHeight * 2
        for b in range(4):
            b = rectWidth * 2
            drawRectangle(height, width)
            tr.penup()
            tr.forward(b)
            tr.pendown()
        tr.penup()
        tr.goto(boardX + rectWidth, boardY + oddRectSpace)
        tr.pendown()

    tr.hideturtle()

# Function to draw the chessboard
def drawChessboard(boardX, boardY, width = 250, height = 250):

# Draw border
    tr.color("Green")
    tr.penup()
    tr.goto(boardX, boardY)
    tr.pendown()
    tr.forward(width)
    tr.left(90)
    tr.forward(height)
    tr.left(90)
    tr.forward(width)
    tr.left(90)
    tr.forward(height)
    tr.left(90)
    tr.color("Black")

# Draw rectangles
    tr.penup()
    tr.goto(boardX, boardY)
    tr.pendown()
    drawAllRectangles(height, width, boardX, boardY)
#Done
    tr.hideturtle()
    turtle.done()